$(document).ready(function(){
    mediaQuerySizing()
});

$(window).resize(function(){
    mediaQuerySizing()
});

function mediaQuerySizing(){
    if ($(window).width() <= 980) {  
        $('.header__menu').hide();
    } else {
        $('.header__menu').show();
    }
}

$('.header__menu-hamburger > .hamburger').click(function(){
    $(this).toggleClass('is-active');
    $('.header__menu').slideToggle();
});