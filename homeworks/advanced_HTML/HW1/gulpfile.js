'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('styles', function () {
    return gulp.src('scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css'));
});

gulp.task('watch', function () {
    gulp.watch('scss/*.scss', gulp.series('styles'));
});

// // ======= //
//
// gulp.task('style', function (done) {
//     return gulp.src('scss/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(cleanCSS())
//         .pipe(gulp.dest('build/css'));
//
//     done();
// });
//
// gulp.task('style:watch', function () {
//     return gulp
//         .watch(
//             'scss/*.scss',
//             gulp.series('style')
//         );
// });
//
