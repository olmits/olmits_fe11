const contactBtn = document.querySelector('.button-contact__us');

contactBtn.addEventListener('click', () => {
    setCookie('experiment', 'noValue', {'max-age': 5});
    (getCookie('new-user') === 'true' ? setCookie('new-user', 'false') : setCookie('new-user', 'true'))
})

function getCookie(name) {    
    let matches =document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {
    let updatedCookie = `${encodeURIComponent(name)}=${encodeURIComponent(value)}`;

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }
    
    let optionPairs = Object.entries(options);
    optionPairs = optionPairs.map((item) => `${item[0]}=${item[1]}`);
    
    updatedCookie += "; " + optionPairs.join('; ');

    document.cookie = updatedCookie;
}