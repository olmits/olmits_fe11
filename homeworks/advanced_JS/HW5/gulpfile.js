'use strict';

const gulp = require('gulp'),
      gp_uglify = require('gulp-uglify-es').default,
      gp_babel = require('gulp-babel'),
      gp_concat = require('gulp-concat'),
      gp_rename = require('gulp-rename'),
      sass = require('gulp-sass'),
      imagemin = require('gulp-imagemin'),
      cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('styles', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('src/css'));
});
gulp.task('watch', function () {
    gulp.watch('src/scss/*.scss', gulp.series('styles'));
});

gulp.task('build', function() {

    // js
    gulp.src('./src/js/*.js')
        .pipe(gp_concat('index.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest('./dest/js'));
        
    // html
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dest'));

    // css
    gulp.src('./src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dest/css'));
    
});
