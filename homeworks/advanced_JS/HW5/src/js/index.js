const url = 'https://swapi.co/api/films/';
let characterUrls = [];


const proceedRequest = function(url, responceCallback){
    let request = new XMLHttpRequest();
    request.responseType = 'json';
    request.addEventListener('load', function() {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this);
            
            let data = this.response.results;
            responceCallback(data);
        }
    });
    request.open('GET', url, true);
    request.send();
    return 'Done'
}

const proceedMultipleRequests = function(urls, elementId, responceCallback){
    let xhr = [], i;
    let dataContainer = document.createElement('div');
    dataContainer.classList.add('row', 'justify-content-start');
    document.querySelector(`.card[data-episode="${elementId}"]`).after(dataContainer);

    if(!Array.isArray(urls)) {
        throw new Error(`Uniterable object: ${urls}`);
    }
    if (urls.length < 1) {
        throw new Error(`Object is empty: ${urls}`);
    }
    for(i = 0; i < urls.length; i++){
        (function(i){
            xhr[i] = new XMLHttpRequest();
            let url = urls[i];
            xhr[i].open('GET', url, true);
            xhr[i].addEventListener('load', function() {
                if (this.readyState === 4 && this.status === 200) {
                    let data = this.response;
                    responceCallback(data, dataContainer);
                };
            });
            xhr[i].send();
        })(i);
    }
}


const proceedEpisodeData = function(epidodeData) {
    const dataContainer = document.querySelector('.film-list');
    console.log(epidodeData);
    

    if (!Array.isArray(epidodeData)) {
        throw new Error(`Uniterable object: ${epidodeData}`);
    }
    epidodeData.sort((a, b) => a.episode_id - b.episode_id);
    epidodeData.forEach((episodeItem) => {
        
        let episodeId = episodeItem.episode_id;
        let episodeCharacters = episodeItem.characters;
        let dataEpisodeLayout = createEpisodeLayout(episodeItem, episodeId);
        
        dataContainer.append(dataEpisodeLayout);
        characterUrls.push({'urls': episodeCharacters, 'id': episodeId});
    });
}

const proceedCharacterData = function(characterData, elementId) {
    let dataParsed = JSON.parse(characterData);
    console.log(dataParsed);
    
    let dataItemCard = document.createElement('div');
    dataItemCard.classList.add('col-6', 'col-lg-4', 'col-md-3', 'card');
    dataItemCard.innerHTML = `
        <div class="card-body character-card"><h6 class="card-subtitle">${dataParsed.name}</h5></div>
        `
    elementId.append(dataItemCard);
}


const createEpisodeLayout = function(itemToWrap, itemIndex) {
    let cardTemplate = document.querySelector('template');
    let itemContainer = cardTemplate.content.cloneNode(true);
    
    let thisContainer = itemContainer.querySelector('.card');
    thisContainer.dataset.episode = itemIndex;
    
    let itemContainerHeader = itemContainer.querySelector('.card-header');
    itemContainerHeader.setAttribute('id', `heading${itemIndex}`);
    
    
    let buttonCollapse = itemContainerHeader.querySelector('button');
    buttonCollapse.dataset.target = `#collapse${itemIndex}`;
    buttonCollapse.setAttribute('aria-controls', `collapse${itemIndex}`);
    buttonCollapse.innerHTML = `Episode: ${itemToWrap.episode_id} ${itemToWrap.title}`;
    
    let itemContainerCollapse = itemContainer.querySelector('.collapse');
    itemContainerCollapse.setAttribute('id', `collapse${itemIndex}`);
    itemContainerCollapse.setAttribute('aria-labelledby', `heading${itemIndex}`);
    
    let itemContainerContent = itemContainerCollapse.querySelector('.card-body');
    itemContainerContent.innerHTML = itemToWrap.opening_crawl;
    
    return itemContainer;
}

proceedRequest(url, proceedEpisodeData);

let awaitRequest = setTimeout(() => {
    characterUrls.forEach((urlSet) => {
        proceedMultipleRequests(urlSet.urls, urlSet.id, proceedCharacterData);
    })
    }, 5000);

