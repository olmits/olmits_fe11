import Board from './board-component.js';
import BoardColumnDraggableCard from './boar-column-draggable-card.js';
import BoardColumnItem from './board-column-item.js';
import BoardColumnAddItem from './board-column-add-item.js';
import BoardException from './board-exception.js';


class BoardColumn extends Board{
    _outterContainer
    _items = [];
    constructor(name) {
        super();
        this._name = name;
        if (typeof(this._name) !== 'string') {
            throw new BoardException(`${BoardColumn.name} must be a string`);
        }
        this._pasteName = this._name.replace(/[_-]/g, ' ');

        this._createLayout('div', {'attrs': {'class': 'list', 'data-list-name': this._name}});
        this.appendTo(this._outterContainer);
        this._addItemFormLayout();
    }
    _createLayout(elementName, elementNodes) {
        super._createLayout(elementName, elementNodes);

        this._outterContainer = $('<div></div>').addClass('list-wrapper');
        this._outterContainer.html(`<h3 class='list-header'>${this._pasteName}</h3>`);
        this._outterContainer[0].dataset.columnName = this._name;
        $('.board-canvas').append(this._outterContainer);

    }
    _addItemFormLayout(){
        this._form = new BoardColumnAddItem((innerText) => this.addItem(innerText));
        this._form.appendTo(this._outterContainer);
    }
    _removeItem(itemToRemove){
        const itemIndex = this._items.findIndex( el => el.item._el === itemToRemove);
        this._items[itemIndex].item.destroy();
        this._items[itemIndex].container.destroy();
        this._items.splice(itemIndex, 1);
    }
    addItem(innerText){
        const item = new BoardColumnItem(innerText, this._removeItem.bind(this));
        const itemContainer = new BoardColumnDraggableCard(item);
        
        itemContainer.appendTo(this._el);
        this._items.push({container: itemContainer, item});
    }
}

export default BoardColumn;
