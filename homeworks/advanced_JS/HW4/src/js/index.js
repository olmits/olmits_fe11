import Board from './board-component.js';
import BoardColumn from './board-column.js';

class BoardApp extends Board {
    constructor(name) {
        super({
            name
        })
        this._initialize()
    }
    _initialize() {
        this._column = new BoardColumn(this._props.name);
    }
}

const toDoList = new BoardApp('to-do');

