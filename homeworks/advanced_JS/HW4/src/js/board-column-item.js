import Board from './board-component.js';

class BoardColumnItem extends Board{
    _removeItemFun = event => {
        this._props.removeItemFun(this._el);
    }
    
    constructor(innerText, removeItemFun){
        super({
            innerText,
            removeItemFun
        })
        this._createLayout('div', {'attrs': {'class': 'item-card'}});
        this._listenRemove();
    }
    _listenRemove() {
        this._remove.addEventListener('click', () => {
            this._removeItemFun();
        })
    }
    _createLayout(elementName, elementNodes) {
        super._createLayout(elementName, elementNodes);
        
        const cardInnerText = $('<span></span>').addClass('item-card__inner-text').text(this._props.innerText)
        this._innerText = cardInnerText[0]

        const cardRemoveButton = $('<button></button>').addClass('item-card__remove-button').html('&#10005;');
        this._remove = cardRemoveButton[0];

        this._el.append(this._innerText);
        this._el.append(this._remove);
    }
    destroy() {
        this._remove.removeEventListener('click', this._removeItemFun);
        super.destroy()
    }
}

export default BoardColumnItem;