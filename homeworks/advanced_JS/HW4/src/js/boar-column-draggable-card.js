import Board from './board-component.js';

let draggableObject = null

class BoardColumnDraggableCard extends Board {
    _handleDragStart = event => {
        draggableObject = this._el;
        event.dataTransfer.effectAllowed = "move";
        event.dataTransfer.setData('text/html', this._el.outerHTML);
        this._el.classList.add('draggable');
    }
    _handleDragEnter = event => {
        event.target.classList.add('over');
    }
    _handleDragOver = event => {
        if (event.preventDefault) {
            event.preventDefault();
        }
        if (event.target.classList.contains('list-card')) {
            let checkResult = this._verifySiblings(draggableObject, event.target);
            console.log(checkResult);
            if (checkResult) {
                event.target.parentNode.insertBefore(draggableObject, event.target)
            } else {
                event.target.parentNode.insertBefore(draggableObject, event.target.nextSibling)
            }
        }
    }
    _handleDragLeave = event => {
        event.target.classList.remove('over');
    }
    _handleDrop = event => {
        event.target.classList.remove('over');
    }
    _handleDragEnd = () => {
        this._el.classList.remove('draggable');
        draggableObject = null;
    }

    constructor(item){
        super()
        this._item = item
        this._createLayout('div', {'attrs': {'class': 'list-card', 'draggable': 'true'}});
    }
    _createLayout(elementName, elementNodes) {
        super._createLayout(elementName, elementNodes);
        this._item.appendTo(this._el);
        this._listenDraggable();
    }
    _listenDraggable() {
        this._el.addEventListener('dragstart', this._handleDragStart, false);
        this._el.addEventListener('dragenter', this._handleDragEnter, false)
        this._el.addEventListener('dragover', this._handleDragOver, false);
        this._el.addEventListener('dragleave', this._handleDragLeave, false);
        this._el.addEventListener('drop', this._handleDrop, false);
        this._el.addEventListener('dragend', this._handleDragEnd, false);
    }
    _verifySiblings(el1, el2) {
        let current
        if (el2.parentNode === el1.parentNode) {
            for(current = el1.previousSibling; current; current = current.previousSibling ) {
                if (current === el2) return true
            }
        } else return false;
    }
    destroy(){
        this._el.removeEventListener('dragstart', this._handleDragStart);
        this._el.removeEventListener('dragenter', this._handleDragEnter)
        this._el.removeEventListener('dragover', this._handleDragOver);
        this._el.removeEventListener('dragleave', this._handleDragLeave);
        this._el.removeEventListener('drop', this._handleDrop);
        this._el.removeEventListener('dragend', this._handleDragEnd);
        this._el.remove();
    }
}

export default BoardColumnDraggableCard;