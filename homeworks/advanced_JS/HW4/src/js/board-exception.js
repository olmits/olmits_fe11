class BoardException extends Error {
    constructor(...args) {
        super(...args);
    }
}

export default BoardException;