import BoardException from './board-exception.js';

class Board {
    _el
    constructor(props){
        this._props = props;
    }
    appendTo(parent){
        parent.append(this._el);
    }
    _createLayout(elementName, elementNodes) {
        if (typeof(elementName) !== 'string') {
            throw new BoardException(`${GameException} must be a string`);
        }
        if (typeof(elementNodes) !== 'object') {
            throw new BoardException(`${GameException} must be an object`);
        }
        const element = document.createElement(elementName);
        if(elementNodes && elementNodes.attrs){
            for(let key in elementNodes.attrs) {
                element.setAttribute(key, elementNodes.attrs[key]);
              }
        }
        this._el = element;
    }
    destroy(){

    }
}

export default Board;