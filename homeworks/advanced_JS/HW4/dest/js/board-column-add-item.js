import Board from './board-component.js';

class BoardColumnAddItem extends Board{
    _onKeyDownFun = event => {
        
        if (this._el.value !== '' && event.keyCode == 13) {
            this._props.addItemFun(this._el.value);
            this._el.value = "";
        }
    }
    constructor(addItemFun){
        super({addItemFun})
        this._createLayout('input', {'attrs': {'class': 'item-composer', 'type': 'text', 'placeholder': 'Add new card'}});
        this._listenItemAdd()
    }
    _listenItemAdd(){
        this._el.addEventListener('keydown', this._onKeyDownFun);
    }
    destroy(){
        this._el.removeEventListener('keydown', this._onKeyDownFun);
        super.destroy();
    }
}

export default BoardColumnAddItem;