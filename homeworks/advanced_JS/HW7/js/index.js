const url = 'https://swapi.co/api/films/';
const dataContainer = document.querySelector('.film-list');


const proceedStarWarsRequest = function(url) {
    fetch(url)
        .then((response) => response.json())
        .then((result) => {
            proceedEpisodeData(result.results);
        })
}

const proceedEpisodeData = function(epidodeData) {

    if (!Array.isArray(epidodeData)) {
        throw new Error(`Uniterable object: ${epidodeData}`);
    }
    epidodeData.sort((a, b) => a.episode_id - b.episode_id);
    epidodeData.forEach((episodeItem) => {
        
        let episodeId = episodeItem.episode_id;
        let episodeCharacters = episodeItem.characters;
        let dataEpisodeLayout = createEpisodeLayout(episodeItem, episodeId);
        
        dataContainer.append(dataEpisodeLayout);
        episodeCharacters = episodeCharacters.map((el) => promiseRequest(el))
        
        Promise.all(episodeCharacters)
            .then(results => {
                results.forEach((result) => proceedCharacterData(result, episodeId))
            })
    });
}

const promiseRequest = function(item) {
    let promise = fetch(item)
                    .then((response) => response.json());
    return promise
}

const proceedCharacterData = function(characterData, episodeId) {
    let cardContainer = document.querySelector(`[data-episode="${episodeId}"]>.card-characters`)
    console.log(cardContainer);
    
    
    let dataItemCard = document.createElement('div');
    dataItemCard.classList.add('col-6', 'col-lg-4', 'col-md-3', 'card');
    dataItemCard.innerHTML = `
        <div class="card-body character-card"><h6 class="card-subtitle">${characterData.name}</h5></div>
    `
    cardContainer.append(dataItemCard);
}


const createEpisodeLayout = function(itemToWrap, itemIndex) {
    let cardTemplate = document.querySelector('template');
    let itemContainer = cardTemplate.content.cloneNode(true);
    
    let thisContainer = itemContainer.querySelector('.card');
    thisContainer.dataset.episode = itemIndex;
    
    let itemContainerHeader = itemContainer.querySelector('.card-header');
    itemContainerHeader.setAttribute('id', `heading${itemIndex}`);
    
    
    let buttonCollapse = itemContainerHeader.querySelector('button');
    buttonCollapse.dataset.target = `#collapse${itemIndex}`;
    buttonCollapse.setAttribute('aria-controls', `collapse${itemIndex}`);
    buttonCollapse.innerHTML = `Episode: ${itemToWrap.episode_id} ${itemToWrap.title}`;
    
    let itemContainerCollapse = itemContainer.querySelector('.collapse');
    itemContainerCollapse.setAttribute('id', `collapse${itemIndex}`);
    itemContainerCollapse.setAttribute('aria-labelledby', `heading${itemIndex}`);
    
    let itemContainerContent = itemContainerCollapse.querySelector('.card-body');
    itemContainerContent.innerHTML = itemToWrap.opening_crawl;

    return itemContainer;
}

proceedStarWarsRequest(url);
