class GameComponent {
    _el = null;
    
    constructor(props) {
        this._props = props;
    }
    appendTo(parent){
        parent.append(this._el);
    }
    _initialize() {}
    _createLayout(elementName, elementNodes) {
        if (typeof(elementName) !== 'string') {
            throw new GameException(`${GameException} is not a string`);
        }
        const element = document.createElement(elementName);
        if(elementNodes && elementNodes.attrs){
            for(let key in elementNodes.attrs) {
                element.setAttribute(key, elementNodes.attrs[key]);
              }
        }
        this._el = element;
    }
    destroyElement() {}
}
class GameTableUI extends GameComponent {
    _cells = [];
    _currentCell;
    _cellAmount = {};
    _passGameScore = (scoreTarget) => {
        this._props.countScore(scoreTarget);
    }
    _returnScore = () => {
        clearInterval(this._gameInterval);
        this._props.endGame()
    }
    constructor(timer, countScore, endGame){
        super({
            timer,
            countScore,
            endGame
        })
        this._createLayout('table', {'attrs': {'class': 'mole_game-table'}});
        this._selectCell();
        this._initInterval();
    }
    _createLayout(elementName, elementNodes){
        super._createLayout(elementName, elementNodes);
        for (let itr = 0; itr < 5; itr++) {
            let tr = document.createElement('tr');
            for (let itd = 0; itd < 5; itd++) {
                const td = new GameCellUI(this._removeFromCells.bind(this));
                td.appendTo(tr);
                this._cells.push(td);
            }
            this._el.append(tr);
        }
        this._cellAmount = Object.assign({}, {cells: this._cells.length});
    }
    _removeFromCells(cellClicked){
        clearInterval(this._gameInterval);
        const cellIndex = this._cells.findIndex( el => el._el === cellClicked);
        (cellClicked.classList.contains('success') ? this._passGameScore(1) : this._passGameScore(0));
        this._destroyElement(cellIndex);
        this._selectCell();
        this._initInterval();
    }
    _initInterval() {
        this._gameInterval = setInterval(this._cellProceed.bind(this), this._props.timer * 1000);
    }
    _cellProceed() {
        
        this._checkClickEvent()
        this._selectCell()
    }
    _checkClickEvent(){
        if (this._currentCell && !this._currentCell._el.classList.contains('success')) {
            this._currentCell._el.classList.add('failure');
            this._currentCell._el.classList.remove('active');
            this._passGameScore(0);
            
            const cellIndex = this._cells.findIndex( el => el === this._currentCell);
            this._destroyElement(cellIndex);
        }
    }
    _selectCell() {
        if (this._cells.length < 1) {
            this._returnScore()
        } else {
            this._cells.map((element) => (element._el !== null ? element._el.classList.remove('active') : null));
            this._currentCell = this._cells[Math.floor(Math.random()*this._cells.length)];
            this._currentCell._el.classList.add('active');
        }
    }
    _destroyElement(cellIndex) {
        this._cells[cellIndex].stopClickListening();
        this._cells.splice(cellIndex, 1);
    }
}
class GameCellUI extends GameComponent {
    _passCellClicked = event => {
        this._props.changeCellOnClick(event.target)
    }
    constructor(changeCellOnClick){
        super({
            changeCellOnClick
        }) 
        this._createLayout('td', {'attrs': {'class': 'mole_game-table-cell'}});
        this._clickHandler = this._checkCellClicked.bind(this);
        this._startClickListening();
    }
    _startClickListening() {
        this._el.addEventListener('click', this._clickHandler)
    }
    stopClickListening() {
        this._el.removeEventListener('click', this._clickHandler);
    }
    _checkCellClicked() {
        if (this._el.classList.contains('active')) {
            this._el.classList.remove('active');
            this._el.classList.add('success');
        } else {
            this._el.classList.add('failure');
        }
        this._passCellClicked(event);
    }
}
class GameApp extends GameComponent {
    _computerScore = 0
    _playerScore = 0
    constructor(){
        super()
        // this._level = level;
        this._initialize();
        this._createLayout('div', {'attrs': {'class': 'mole_game'}});
    }
    static levelDiff = {'easy': 1.5, 'medium': 1, 'hard': 0.5};
    _initialize() {
        const level = prompt('Choose a level: easy, medium, hard');
        if (!(level in GameApp.levelDiff)) {
            throw new GameException(`${level} is not a level`);
        }
        const timer = GameApp.levelDiff[level];
        this._table = new GameTableUI(timer, this._receiveScore.bind(this), this._endGame.bind(this));
    }
    _createLayout(elementName, elementNodes){
        super._createLayout(elementName, elementNodes);
        this._table.appendTo(this._el);
    }
    _receiveScore(x) {
        (x === 1 ? this._playerScore++ : this._computerScore++);
    }
    _endGame() {
        document.querySelector('.mole_game').remove();
        alert(`Game is ended: player - ${this._playerScore}, computer - ${this._computerScore}`);
        const moleGame = new GameApp();
        moleGame.appendTo(gameContainer);

    }
}
class GameException extends Error {
    constructor(...args) {
        super(...args);
    }
}
const moleGame = new GameApp();
const gameContainer = document.querySelector('#game');
moleGame.appendTo(gameContainer)
