'use strict';

const gulp = require('gulp'),
      uglify = require('gulp-uglify-es').default,
      babel = require('gulp-babel'),
      concat = require('gulp-concat'),
      sass = require('gulp-sass'),
      imagemin = require('gulp-imagemin'),
      cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('styles', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('src/css'));
});
gulp.task('watch', function () {
    gulp.watch('src/scss/*.scss', gulp.series('styles'));
});

gulp.task('build', function() {

    // js
    gulp.src('./src/js/*.js')
        .pipe(babel({
            presets: ['@babel/preset-env'],
            plugins: ["@babel/plugin-proposal-class-properties"]
            }))
        // .pipe(concat('index.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dest/js'));
        
    // html
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dest'));

    // css
    gulp.src('./src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dest/css'));
    
    // image
    gulp.src('./src/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dest/img'))

});
