import {RequestHelper} from "./request-helper.js";

export class TwitterCardRequest {

    async getTwitterCards() {
        const twitterPosts = await RequestHelper.GetDeleteData('GET', 'https://jsonplaceholder.typicode.com/posts');
        const twitterUsers = await RequestHelper.GetDeleteData('GET', 'https://jsonplaceholder.typicode.com/users');
        
        return {cards: twitterPosts, users: twitterUsers}
    }
    async updateTwitterCard(obj, cardID) {
        console.log(cardID);
        
        const editTwitterCardResp = await RequestHelper.PostPutData('PUT', `https://jsonplaceholder.typicode.com/posts/${cardID}`, obj);
        console.log('PUT_resp', editTwitterCardResp);

        return {card: editTwitterCardResp}
    }
    async createTwitterCard(obj) {
        const createTwitterCardResp = await RequestHelper.PostPutData('POST', `https://jsonplaceholder.typicode.com/posts/`, obj);
        return {card: createTwitterCardResp}
    }
    async deleteTwitterCard(cardID) {
        const deleteTwitterCardResp = await RequestHelper.GetDeleteData('DELETE', `https://jsonplaceholder.typicode.com/posts/${cardID}`);

        console.log('DEL resp', deleteTwitterCardResp);
        return {card: deleteTwitterCardResp}
    }
}   