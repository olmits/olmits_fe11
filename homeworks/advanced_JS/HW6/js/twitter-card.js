import {TwitterException} from "./twitter-component.js";
import {TwitterCardRequest} from "./twitter-card-request.js";

export class TwitterCard {
    
    _el

    constructor(cardId, cardTitle, cardBody, userObj=null) {
        this._cardId = cardId;
        this._cardTitle = cardTitle;
        this._cardBody = cardBody;
        this._userObj = userObj
        this._twitterRequest = new TwitterCardRequest();
        this._verifyUser();
    }
    _verifyUser() {
        if (this._userObj === null){
            this._userObj = {
                id: 1,
                name: "Leanne Graham"
            }
        }
    }
    _createElement(elementName, ...args) {
        if (typeof(elementName) !== 'string') {
            throw new TwitterException(`Error in TwitterCard._createElement - invalid data object in ${elementName}`);
        }
        const element = document.createElement(elementName);
        const elemnetClassValue = args.join(' ');
        element.setAttribute('class', elemnetClassValue);
        if(elementName === 'button') element.setAttribute('type', 'button');
        
        return element;
    }
    editLayout() {
        const cardLayout = document.getElementById(this._cardId);
        
        const cardLayoutBodyTitle = cardLayout.querySelector('.card-title');
        cardLayoutBodyTitle.innerText = this._cardTitle
        
        const cardLayoutBodyText = cardLayout.querySelector('.card-text');
        cardLayoutBodyText.innerText = this._cardBody

        const cardLayoutBodyEditBtn = cardLayout.querySelector('.card-edit');
        cardLayoutBodyEditBtn.dataset.cardTitle = this._cardTitle;
        cardLayoutBodyEditBtn.dataset.cardBody = this._cardBody;
    }
    createLayout() {
        
        const cardLayout = this._createElement('div', 'card', 'my-2');
        cardLayout.id = this._cardId;

        const cardLayoutBody = this._createElement('div', 'card-body', 'd-flex', 'flex-column', 'align-items-start');
        
        const cardLayoutBodyTitle = this._createElement('h5', 'card-title');
        cardLayoutBodyTitle.innerText = this._cardTitle;
        cardLayoutBody.append(cardLayoutBodyTitle);
        
        const cardLayoutBodyText = this._createElement('p', 'card-text');
        cardLayoutBodyText.innerText = this._cardBody;
        cardLayoutBody.append(cardLayoutBodyText);
        
        const cardLayoutBodyEditBtn = this._createElement('button', 'card-edit', 'btn', 'btn-secondary', 'px-4', 'align-self-end');
        cardLayoutBodyEditBtn.innerText = 'Edit'
        cardLayoutBodyEditBtn.dataset.toggle = "modal";
        cardLayoutBodyEditBtn.dataset.target = "#twitterCardModal";

        cardLayoutBodyEditBtn.dataset.cardUserName = this._userObj.name;
        cardLayoutBodyEditBtn.dataset.cardUserId = this._userObj.id;
        cardLayoutBodyEditBtn.dataset.cardTitle = this._cardTitle;
        cardLayoutBodyEditBtn.dataset.cardBody = this._cardBody;
        cardLayoutBodyEditBtn.dataset.cardId = this._cardId;
        cardLayoutBodyEditBtn.dataset.formMethod = 'PUT';

        this._cardLayoutBodyDeleteBtn = this._createElement('button', 'btn', 'btn-warning', 'px-4', 'align-self-end');
        this._cardLayoutBodyDeleteBtn.innerText = 'Delete'

        cardLayoutBody.append(cardLayoutBodyEditBtn);
        cardLayoutBody.append(this._cardLayoutBodyDeleteBtn);


        const cardLayoutFooter = this._createElement('div', 'card-footer', 'd-flex', 'flex-column', 'align-items-end');
        cardLayoutFooter.innerHTML = `
            <small class="text-muted">${this._userObj.name} - ${this._userObj.email}</small>
            `
        
        cardLayout.append(cardLayoutBody);
        cardLayout.append(cardLayoutFooter);
        
        this._el = cardLayout;
        this._listenRemove();
    }
    async _handleRemoving(){
        await this._twitterRequest.deleteTwitterCard(this._cardId);
        this.destroy()
    }
    _listenRemove(){
        this._handleRemoving = this._handleRemoving.bind(this);
        this._cardLayoutBodyDeleteBtn.addEventListener('click', this._handleRemoving);
    }
    prependTo(parent){
        parent.prepend(this._el)
    }
    destroy() {
        this._el.remove();
        this._cardLayoutBodyDeleteBtn.removeEventListener('click', this._handleRemoving);
    }
}