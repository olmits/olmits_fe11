import {TwitterException} from "./twitter-component.js";

export class RequestHelper {
    
    _handleError(response){
        if (!response.ok) {
            throw new TwitterException(response.statusText);
        }
        return response;
    }

    static async GetDeleteData(method, url) {
        return await fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        })
        .then(this._handleError)
        .then((response) => response.json())
        .catch(function(error){
            console.log(error);
        })
    }
    static async PostPutData(method, url, obj) {
        return await fetch(url, {
            method: method,
            headers:{
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(obj)
        })
        .then(this._handleError)
        .then((response) => response.json())
        .catch(function(error){
            console.log(error);
        })
    }
}