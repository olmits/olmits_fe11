import {TwitterCardRequest} from "./twitter-card-request.js";
import {twitterFeedContainer} from "./twitter-component.js";
import {TwitterCard} from "./twitter-card.js";

export default class TwitterForm {

    _tData
    
    constructor(form){
        this._form = form;
        
        this._twitterRequest = new TwitterCardRequest();
    }
    async proceedSubmit(event) {

        if (event.preventDefault) {
            event.preventDefault();
        }
        this._requestMethod = this._form.getAttribute('method');
        this._formData = new FormData(this._form);
        this._userData = Object.fromEntries(this._formData);
        
        switch (this._requestMethod) {
            case 'PUT':
                this._tData = await this._twitterRequest.updateTwitterCard(this._userData, this._userData.id);
                
                const twitterCardToEdit = new TwitterCard(this._tData.card.id, this._tData.card.title, this._tData.card.body);
                twitterCardToEdit.editLayout();
                break
            case 'POST':
                this._tData = await this._twitterRequest.createTwitterCard(this._userData);
                console.log(this._tData);
                
                const twitterCardToAdd = new TwitterCard(this._tData.card.id, this._tData.card.title, this._tData.card.body);
                twitterCardToAdd.createLayout();
                twitterCardToAdd.prependTo(twitterFeedContainer);

                break
        }

        
    }
    _handleFormSubmit() {
        this.proceedSubmit = this.proceedSubmit.bind(this);
        this._form.addEventListener('submit', this.proceedSubmit);
    }
    init() {
        this._handleFormSubmit();
    }
}