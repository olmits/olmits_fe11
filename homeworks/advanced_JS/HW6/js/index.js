import {TwitterCardRequest} from "./twitter-card-request.js";
import {TwitterException, twitterFeedContainer} from "./twitter-component.js";
import {TwitterCard} from "./twitter-card.js";
import TwitterForm from "./twitter-modal.js";

class TwitterCards {

    _arrOfPosts = [];
    _arrOfUsers = [];

    
    async init() {
        this._twitterRequest = new TwitterCardRequest();
        
        await this._renderCards();
    }

    _prepareTwitterPost(item) {
        if (typeof(item) !== 'object' || item === null) {
            throw new TwitterException(`Error in TwitterCards._prepareTwitterPost - invalid data object in ${item}`);
        }

        const userObj = this._arrOfUsers.find(element => element.id === item.userId);
        const twitCard = new TwitterCard(item.id, item.title, item.body, userObj);
        twitCard.createLayout()
        twitCard.prependTo(twitterFeedContainer);
    }

    async _renderCards() {
        const arrOfCards = await this._twitterRequest.getTwitterCards();
        console.log(arrOfCards);
        
        this._arrOfPosts = arrOfCards.cards; 
        this._arrOfUsers = arrOfCards.users;

        this._arrOfPosts.forEach(item => this._prepareTwitterPost(item));
    }
}

async function init() {
    const start = new TwitterCards();
    await start.init();

    const modalForm = document.getElementById('tweet-form');
    
    const twitterForm = new TwitterForm(modalForm);
    twitterForm.init();
}

$('#twitterCardModal').on('show.bs.modal', function (event) {
    /**
     * Iniate modal window on EDIT button clicked
     * All data uploading inside form inputs from a BUTTON datasets
     */
    const button = $(event.relatedTarget);
    
    const userData = {};
    if ('cardUserName' in button[0].dataset) {
        userData.cardUserName = button.data('card-user-name');
    }else {
        userData.cardUserName = 'Current User';
    }
    (('cardUserId' in button[0].dataset) ? userData.cardUserId = button.data('card-user-id') : userData.cardUserName = 0 );
    (('cardId' in button[0].dataset) ? userData.cardId = button.data('card-id') : userData.cardId = 0);
    (('cardTitle' in button[0].dataset) ? userData.cardTitle = button.data('card-title') : userData.cardTitle = '');
    (('formMethod' in button[0].dataset) ? userData.formMethod = button.data('form-method') : userData.formMethod = '');
    (('cardBody' in button[0].dataset) ? userData.cardBody = button.data('card-body') : userData.cardBody = '');
    
    console.log(userData);
    
    const modal = $(this)
    modal.find('.modal-title').text(`Tweeted by ${userData.cardUserName}`);
    modal.find('[name="userId"]').val(userData.cardUserId);
    modal.find('[name="id"]').val(userData.cardId);
    modal.find('[name="title"]').val(userData.cardTitle);
    modal.find('[name="body"]').val(userData.cardBody);
    modal.find('#tweet-form > [type="submit"]').val(button[0].innerText);
    
    modal.find('#tweet-form').attr('data-card-id', userData.cardId);
    modal.find('#tweet-form').attr('method', userData.formMethod);

})

init();
