export const twitterFeedContainer = document.querySelector('.twitter_feed');
export const twitterItems = [];

export class TwitterException extends Error {
    constructor(...args) {
        super(...args);
    }
}
