$(document).ready(function() {
    $('.ipinfo').click(function() {
        $(this).prop('disabled', true);
        $(this).html(
            `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
        )
        $.when( getUser(urlIp, urlIpInfo) ).done(function() {
            $('.ipinfo')
                .html(`<span>Вычислить по IP</span>`)
                .prop('disabled', false);
        })
    })
})

const urlIp = 'https://api.ipify.org/';
const urlIpParams = {format: 'json'};

const urlIpInfo = 'http://ip-api.com/json/';
const urlIpInfoParams = {lang:'ru', fields:'country,regionName,city,zip,lat,lon,query'};
const prmsTranslation = {country: 'Страна', regionName: 'Регион', city: 'Город', zip: 'Индекс',lat: 'Широта',lon: 'Долгота',query: 'IP адресс'}

const tableToShowInfo = document.querySelector('.ipinfo-content')

async function getUser(urlGetIp, urlGetIpInfo) {
    try {
        const userIpReq = await getUserInfo(urlGetIp, urlIpParams);
        const userIpInforReq = await getUserInfo(urlGetIpInfo + userIpReq.ip, urlIpInfoParams)
        proceedUserInfo(userIpInforReq);    
    } catch (error) {
        throw new Error(`getUser() failed: ${error}`)
    }
}

async function getUserInfo(urlS, params={}) {
    let url = new URL(urlS)
    url.search = new URLSearchParams(params).toString();
    
    try {
        const response = await fetch(url);
        return await response.json();
        
    } catch (error) {
        throw new Error(`getUserInfo() failed: ${error}`)
    }
}

function proceedUserInfo(obj){
    let objArr = Object.entries(obj);
    let elemArr = objArr
        .map((item) => {
            return `<tr><td>${prmsTranslation[item[0]]}</td><td>${item[1]}</td></tr>`
        })
        .join('');
    tableToShowInfo.innerHTML = elemArr;
}
