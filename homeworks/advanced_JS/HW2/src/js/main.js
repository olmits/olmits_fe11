try {
    // маленький гамбургер с начинкой из сыра
    const hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);

    // добавка из майонеза
    hamburger.toppings = Hamburger.TOPPING_MAYO;
    
    // спросим сколько там калорий
    console.log("How much calories? Calories: %f", hamburger.calculateCalories());
    
    // сколько стоит
    console.log("How much it would cost? Price: %f", hamburger.calculatePrice());
    
    // я тут передумал и решил добавить еще приправу
    hamburger.toppings = Hamburger.TOPPING_SPICE;

    // А сколько теперь стоит? 
    console.log("Let add another one topping. Price? Price with sauce: %f", hamburger.calculatePrice());

    // Проверить, большой ли гамбургер? 
    console.log("Is hamburger large: %s", hamburger.size === Hamburger.SIZE_LARGE); // -> TRUE
    
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Let's remove last one topping. Have %d toppings now", hamburger.toppings.length); // 1

} catch (error) {
    console.log(`Error: ${error.stack}`);
}