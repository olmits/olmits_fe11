
/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
class Hamburger {
    
    constructor(size, stuffing){
        this.size = size
        this.stuffing = stuffing;
        // this._verifyInput();
    }

/* Размеры, виды начинок и добавок */
    static SIZE_SMALL = {price: 50, calories: 20, name: 'SIZE_SMALL'};
    static SIZE_LARGE = {price: 100, calories: 40, name: 'SIZE_LARGE'}
    static STUFFING_CHEESE = {price: 10, calories: 20, name: 'STUFFING_CHEESE'};
    static STUFFING_SALAD = {price: 20, calories: 5, name: 'STUFFING_SALAD'}
    static STUFFING_POTATO = {price: 15, calories: 10, name: 'STUFFING_POTATO'};
    static TOPPING_MAYO = {price: 20, calories: 5, name: 'TOPPING_MAYO'};
    static TOPPING_SPICE = {price: 15, calories: 0, name: 'TOPPING_SPICE'};
/**
 * Узнать размер гамбургера
 */
    get size(){
        return this._size;
    }
    
    set size(newSize){
        if (!newSize) {
            throw new HamburgerException('no size given');
        }
        if (newSize !== this.constructor.SIZE_LARGE && 
            newSize !== this.constructor.SIZE_SMALL) {
                throw new HamburgerException(`invalid size '${this._size.name}'`);
            }
            this._size = newSize;
        }
/**
 * Узнать начинку гамбургера
 */
    get stuffing() {
        return this._stuffing;
    }
    set stuffing(newStyffing) {
        if (!newStyffing) {
            throw new HamburgerException('no stuffing given');
        }
        if (newStyffing !== this.constructor.STUFFING_CHEESE && 
            newStyffing !== this.constructor.STUFFING_SALAD && 
            newStyffing !== this.constructor.STUFFING_POTATO) {
                throw new HamburgerException(`invalid stuffing '${this._stuffing.name}'`);
        }  
        this._stuffing = newStyffing;
    }

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
    set toppings(topping) {
        (!this._topping ? this._topping = [] : null)
        
        if(typeof topping !== 'object' || topping === null || !topping.name){
            throw new HamburgerException(`invalid value '${topping}'`);
        }
        if(topping.name !== 'TOPPING_MAYO' && topping.name !== 'TOPPING_SPICE'){
            throw new HamburgerException(`invalid topping '${topping.name}'`);
        }
        if(this._topping.includes(topping)){
            throw new HamburgerException(`dublicate topping '${topping.name}'`);
        }
        this._topping.push(topping);
    }
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
    get toppings() {
        return (this._topping ? this._topping : null)
    }
/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
    removeTopping(topping) {
        if(!this._topping){
            throw new HamburgerException(`no topping at all`);
        }
        if(!this._topping.includes(topping)){
            throw new HamburgerException(`no such topping '${topping.name}'`);
        }
        let toppingIndex = this._topping.indexOf(topping);
        this._topping.splice(toppingIndex, 1);
    }
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
    calculatePrice(){
        let price = this._size.price + this._stuffing.price;
        (this._topping ? this._topping.forEach((topping) => price += topping.price) : null)
    return price
    }
/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
    calculateCalories(){
        let calories = this._size.calories + this._stuffing.calories;
        (this._topping ? this._topping.forEach((topping) => calories += topping.calories) : null)
    return calories
    }
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */

class HamburgerException extends Error {
    constructor(...args) {
        super(...args);
    }
}