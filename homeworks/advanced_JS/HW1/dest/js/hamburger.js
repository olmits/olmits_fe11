/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing){
    this._size = size;
    this._stuffing = stuffing;
    if (!size) {
        throw new HamburgerException('no size given');
    }
    if (!stuffing) {
        throw new HamburgerException('no stuffing given')
    }
    if (size !== Hamburger.SIZE_LARGE && 
        size !== Hamburger.SIZE_SMALL) {
        throw new HamburgerException(`invalid size '${size.name}'`);
    }
    if (stuffing !== Hamburger.STUFFING_CHEESE && 
        stuffing !== Hamburger.STUFFING_SALAD && 
        stuffing !== Hamburger.STUFFING_POTATO) {
        throw new HamburgerException(`invalid stuffing '${stuffing.name}'`);
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {price: 50, calories: 20, name: 'SIZE_SMALL'}
Hamburger.SIZE_LARGE = {price: 100, calories: 40, name: 'SIZE_LARGE'}
Hamburger.STUFFING_CHEESE = {price: 10, calories: 20, name: 'STUFFING_CHEESE'}
Hamburger.STUFFING_SALAD = {price: 20, calories: 5, name: 'STUFFING_SALAD'}
Hamburger.STUFFING_POTATO = {price: 15, calories: 10, name: 'STUFFING_POTATO'}
Hamburger.TOPPING_MAYO = {price: 20, calories: 5, name: 'TOPPING_MAYO'}
Hamburger.TOPPING_SPICE = {price: 15, calories: 0, name: 'TOPPING_SPICE'}

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (topping) {
    if(!this._topping){
        this._topping = [];
    }
    if(this._topping.includes(topping)){
        throw new HamburgerException(`dublicate topping '${topping.name}'`);
    }
    this._topping.push(topping);
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if(!this._topping){
        throw new HamburgerException(`no topping at all`);
    }
    if(!this._topping.includes(topping)){
        throw new HamburgerException(`no such topping '${topping.name}'`);
    }
    const toppingIndex = this._topping.indexOf(topping);
    this._topping.splice(toppingIndex, 1);
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function (){
    return this._topping;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function (){
    return this._size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function (){
    return this._stuffing;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function (){
    this._price = this._size.price + this._stuffing.price;
    if(this._topping){
        this._topping.forEach((topping) => this._price += topping.price);
    }
    return this._price
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
    this._calories = this._size.calories + this._stuffing.calories;
    if(this._topping){
        this._topping.forEach((topping) => this._calories += topping.calories);
    }
    return this._calories
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message){
    this.name = 'HamburgerException';
    this.message = `${this.name}: ${message}`;
}


const h = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
h.addTopping(Hamburger.TOPPING_MAYO);
